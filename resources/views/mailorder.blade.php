<h2>Hola {{ $data_email['name'] }}</h2>
<h3>Los siguientes son los datos de tu orden de pedido.</h3>
<p><b>Identificación:</b> {{ $data_email['nit'] }}</p>
<p><b>Planeta:</b> {{ $data_email['planet'] }}</p>
<p><b>Dirección:</b> {{ $data_email['address'] }}</p>
<p><b>Teléfono:</b> {{ $data_email['phone'] }}</p>
<p><b>Servicio:</b> {{ $data_email['service'] }}</p>
<p><b>Costo del servicio:</b> {{ $data_email['value'] }}</p>
<p><b>Decripción del servicio:</b> {{ $data_email['description'] }}</p>
<p><b>Cantidad de días:</b> {{ $data_email['quantity'] }}</p>