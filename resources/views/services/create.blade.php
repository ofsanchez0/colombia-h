@extends('layouts.app', ['activePage' => 'service-management', 'titlePage' => __('Gestionar servicios')])

@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <form method="post" action="{{ route('services.store') }}" autocomplete="off" class="form-horizontal" enctype="multipart/form-data">
            @csrf
            @method('post')

            <div class="card ">
              <div class="card-header card-header-primary">
                <h4 class="card-title">{{ __('Agregar servicio') }}</h4>
                <p class="card-category"></p>
              </div>
              <div class="card-body ">
                <div class="row">
                  <div class="col-md-12 text-right">
                      <a href="{{ route('services.index') }}" class="btn btn-sm btn-primary">{{ __('Volver a la lista') }}</a>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Nombre del servicio') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" id="input-name" type="text" placeholder="{{ __('Nombre del servicio') }}" value="{{ old('name') }}" required="true" aria-required="true"/>
                      @if ($errors->has('name'))
                        <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('name') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Valor') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('value') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('value') ? ' is-invalid' : '' }}" name="value" id="input-value" type="number" placeholder="{{ __('Valor por día del servicio') }}" value="{{ old('value') }}" required="true" aria-required="true"/>
                      @if ($errors->has('value'))
                        <span id="value-error" class="error text-danger" for="input-value">{{ $errors->first('value') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
                  <div class="row">
                    <label class="col-sm-2 col-form-label">{{ __('Descripción') }}</label>
                    <div class="col-sm-7">
                      <div class="form-group">
                        <textarea class="form-control" name="description" id="input-description" placeholder="{{ __('Descripción del servicio') }}"></textarea>
                      </div>
                    </div>
                  </div>
                


              </div>
              <div class="card-footer ml-auto mr-auto">
                <button type="submit" class="btn btn-primary">{{ __('Crear servicio') }}</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection