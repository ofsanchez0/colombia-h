@extends('layouts.app', ['activePage' => 'user-management', 'titlePage' => __('Gestionar usuarios')])

@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <form method="post" action="{{ route('user.store') }}" autocomplete="off" class="form-horizontal" enctype="multipart/form-data">
            @csrf
            @method('post')

            <div class="card ">
              <div class="card-header card-header-primary">
                <h4 class="card-title">{{ __('Agregar usuario') }}</h4>
                <p class="card-category"></p>
              </div>
              <div class="card-body ">
                <div class="row">
                  <div class="col-md-12 text-right">
                      <a href="{{ route('user.index') }}" class="btn btn-sm btn-primary">{{ __('Volver a la lista') }}</a>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Tipo de cliente') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('tipo') ? ' has-danger' : '' }}">
                      <select id="tipo" class="form-control" name="tipo" id="input-tipo" type="text" placeholder="{{ __('Tipo de Cliente') }}" value="">
                        <option value="">Seleccione</option>
                        <option value="1">Persona</option>
                        <option value="2">Empresa</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Nombre') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" id="input-name" type="text" placeholder="{{ __('Nombre') }}" value="{{ old('name') }}" required="true" aria-required="true"/>
                      @if ($errors->has('name'))
                        <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('name') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Nit o cédula') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('nit') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('nit') ? ' is-invalid' : '' }}" name="nit" id="input-nit" type="number" placeholder="{{ __('Nit') }}" value="{{ old('nit') }}" required="true" aria-required="true"/>
                      @if ($errors->has('nit'))
                        <span id="nit-error" class="error text-danger" for="input-nit">{{ $errors->first('nit') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
                  <div class="row">
                    <label class="col-sm-2 col-form-label">{{ __('Rol') }}</label>
                    <div class="col-sm-7">
                      <div class="form-group">
                        <select id="opciones_info" class="form-control" name="id_rol" id="input-rol" type="text" placeholder="{{ __('Rol') }}" value="">
                          <option value="">Seleccione</option>
                          @foreach($roles as $rol)
                          <option value="{{$rol->id}}">{{$rol->name}}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <label class="col-sm-2 col-form-label">{{ __('Planeta') }}</label>
                    <div class="col-sm-7">
                      <div class="form-group{{ $errors->has('planeta') ? ' has-danger' : '' }}">
                        <select id="planeta" class="form-control" name="planeta" id="input-planeta" type="text" placeholder="{{ __('Planeta') }}" value="">
                          <option value="">Seleccione</option>
                          <option value="1">Tierra</option>
                          <option value="2">Marte</option>
                          <option value="3">Jupiter</option>
                          <option value="4">Saturno</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Dirección') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('direccion') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('direccion') ? ' is-invalid' : '' }}" name="direccion" id="input-direccion" type="text" placeholder="{{ __('Dirección') }}" value="{{ old('direccion') }}" required="true" aria-required="true"/>
                      @if ($errors->has('direccion'))
                        <span id="direccion-error" class="error text-danger" for="input-direccion">{{ $errors->first('direccion') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Teléfono') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('telefono') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('telefono') ? ' is-invalid' : '' }}" name="telefono" id="input-telefono" type="number" placeholder="{{ __('Teléfono') }}" value="{{ old('telefono') }}" required="true" aria-required="true"/>
                      @if ($errors->has('telefono'))
                        <span id="telefono-error" class="error text-danger" for="input-telefono">{{ $errors->first('telefono') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Email') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" id="input-email" type="email" placeholder="{{ __('Email') }}" value="{{ old('email') }}" required />
                      @if ($errors->has('email'))
                        <span id="email-error" class="error text-danger" for="input-email">{{ $errors->first('email') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
                
                <div class="row">
                  <label class="col-sm-2 col-form-label" for="input-password">{{ __(' Contraseña') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('password') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" input type="password" name="password" id="input-password" placeholder="{{ __('Contraseña') }}" value="" required />
                      @if ($errors->has('password'))
                        <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('password') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label" for="input-password-confirmation">{{ __('Confirmar contraseña') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input class="form-control" name="password_confirmation" id="input-password-confirmation" type="password" placeholder="{{ __('Confirmar contraseña') }}" value="" required />
                    </div>
                  </div>
                </div>


              </div>
              <div class="card-footer ml-auto mr-auto">
                <button type="submit" class="btn btn-primary">{{ __('Crear usuario') }}</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection