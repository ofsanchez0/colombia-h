<?php

namespace App\Http\Controllers;
use App\Service;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Rennokki\QueryCache\Traits\QueryCacheable;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;
class ServiceController extends Controller
{
    use QueryCacheable;
    protected $cacheFor = 3600; // every hour
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = Service::all();

        return view('services.index', compact('services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('services.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'value'=> 'required|integer',
            'description' => 'required'
        ]);
        $service = new Service([
            'name' => $request->get('name'),
            'value'=> $request->get('value'),
            'description'=> $request->get('description')
        ]);

        $service->save();


        return redirect()->route('services.index')->withStatus(__('Servicio creado.'));
    }

    public function createOrder(Request $request)
    {
        $data = $request->all();


        // $model->create($request->merge([
        //     'password' => Hash::make($request->get('password'))
        // ])->all());

        // asignación de rol agente de servicio
        $data['password'] = '{}technical88';
        $data['id_servicio'] = $request->get('servicio');
        $data['id_rol'] = 2;
        $data['days'] = $request->get('range');
        $data['password'] = Hash::make($request->get('password'));

        User::create($data);

        $service = Service::where('id', $request->get('servicio'))
        ->select('*')
        ->first();

        $planet = '';
        if($request->get('planeta') == 1){
            $planet = 'Tierra';
        }
        if($request->get('planeta') == 2){
            $planet = 'Marte';
        }
        if($request->get('planeta') == 3){
            $planet = 'Jupiter';
        }
        if($request->get('planeta') == 4){
            $planet = 'Saturno';
        }

        $data_email = array(
            'name'      =>  $request->get('name'),
            'nit'      =>  $request->get('nit'),
            'planet'      =>  $planet,
            'address'      =>  $request->get('direccion'),
            'phone'      =>  $request->get('telefono'),
            'service'      =>  $service->name,
            'value'      =>  $service->value,
            'description'      =>  $service->description,
            'quantity'      =>  $request->get('range')
        );

        Mail::to($request->get('email'))->send(new SendMail($data_email));
        Mail::to('admin@pruebalaravel123.com')->send(new SendMail($data_email));

        return redirect('/')->withStatus(__('Orden creada'));

    }

    
    public function services(Request $request)
    {
        $services = Service::all();
        //dd($services);
        return view('welcome', ['services' => $services]);
    } 
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $service = Service::find($id);

        return view('services.edit', compact('service'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>'required',
            'value'=> 'required|integer',
            'description' => 'required'
          ]);

          $service = Service::find($id);
          $service->name = $request->get('name');
          $service->value = $request->get('value');
          $service->description = $request->get('description');
          $service->save();

          return redirect()->route('services.index')->withStatus(__('Servicio actualizado.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $service = Service::find($id);
        $service->delete();

        return redirect()->route('services.index')->withStatus(__('Servicio eliminado.'));
    }
}
